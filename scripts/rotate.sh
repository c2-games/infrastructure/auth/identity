#!/bin/bash
set -e

PASS=$(openssl rand -base64 32)
sed -i "s|KC_DB_PASSWORD=\".*\"|KC_DB_PASSWORD=\"$PASS\"|g" .env.keycloak
sed -i "s|POSTGRES_PASSWORD=\".*\"|POSTGRES_PASSWORD=\"$PASS\"|g" .env.postgres
docker compose exec postgres psql -U keycloak -c "ALTER USER keycloak PASSWORD '$PASS';"
docker compose up -d