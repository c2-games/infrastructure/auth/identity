#!/bin/bash

set -e

mkdir -p /opt/identity/backups
docker compose exec postgres pg_dump -U keycloak | gzip > /opt/identity/backups/dump-$(date +%Y-%m-%d-%H:%M:%S).tar.gz