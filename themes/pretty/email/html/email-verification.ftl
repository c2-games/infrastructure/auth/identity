<html>

<head>
    <style>
        img {
            display: block;
            margin-left: auto;
            margin-right: auto;
        }

        svg {
            margin-left: auto;
            margin-right: auto;
        }

        div.center {
            display: block;
            text-align: center;
        }

        p.center {
            text-align: center;
        }
    </style>
</head>

<body>

    <p class="center"> ${kcSanitize(msg("emailVerificationBodyHtml",link, linkExpiration,
        realmName,linkExpirationFormatter(linkExpiration)))?no_esc} </p>
        <p>You are receiving this email as a registered user at N-CAE Cyber Games.</p>
        <p><a href="mailto:registration@ncaecybergames.org">Unsubscribe</a></p>
</body>

</html>
