<#import "template.ftl" as layout>
<@layout.mainLayout active='account' bodyClass='user'; section>

    <div class="row">
        <div class="col-md-10">
            <h2>Welcome to NCAE CYBERGAMES</h2>
        </div>
    </div>
    <div class="row">
        <div class="column">
            <h2>Important Links</h2>
            <h4>To get started please go to the <a href="https://ui.sandbox.ncaecybergames.org/"><button class="btn-primary btn-lg">Sandbox</button></a></h4>
            <h4>Discord Registration <a href="https://discord.gg/sBnmdKQuZB"><i class="fab fa-discord fa-3x"></i></a></h4>
            <#if account.attributes.registration_code?has_content>
                <h4> Discord Registration Code: ${(account.attributes.registration_code!'')}</h4>
            </#if>
        </div>
        <div class="column">
            <h2> Competition Information</h2>
            <h4>Qualification Status: Congratulations! You are qualified for the event!</h4>
        </div>
    </div>

    <form action="${url.accountUrl}" class="form-horizontal" method="post">
        <h3>Please review the information below to ensure your account information is accurate.</h3>

        <input type="hidden" id="stateChecker" name="stateChecker" value="${stateChecker}">

        <#if !realm.registrationEmailAsUsername>
            <div class="form-group ${messagesPerField.printIfExists('username','has-error')}">
                <div class="col-sm-2 col-md-2">
                    <label for="username" class="control-label">${msg("username")}</label> <#if realm.editUsernameAllowed><span class="required">*</span></#if>
                </div>

                <div class="col-sm-10 col-md-10">
                    <input type="text" class="form-control" disabled id="username" name="username" <#if !realm.editUsernameAllowed>disabled="disabled"</#if> value="${(account.username!'')}"/>
                </div>
            </div>
        </#if>

        <div class="form-group ${messagesPerField.printIfExists('email','has-error')}">
            <div class="col-sm-2 col-md-2">
            <label for="email" class="control-label">${msg("email")}</label> <span class="required">*</span>
            </div>

            <div class="col-sm-10 col-md-10">
                <input type="text" class="form-control" disabled id="email" name="email" autofocus value="${(account.email!'')}"/>
            </div>
        </div>

        <div class="form-group ${messagesPerField.printIfExists('firstName','has-error')}">
            <div class="col-sm-2 col-md-2">
                <label for="firstName" class="control-label">${msg("firstName")}</label> <span class="required">*</span>
            </div>

            <div class="col-sm-10 col-md-10">
                <input type="text" class="form-control" disabled id="firstName" name="firstName" value="${(account.firstName!'')}"/>
            </div>
        </div>

        <div class="form-group ${messagesPerField.printIfExists('lastName','has-error')}">
            <div class="col-sm-2 col-md-2">
                <label for="lastName" class="control-label">${msg("lastName")}</label> <span class="required">*</span>
            </div>

            <div class="col-sm-10 col-md-10">
                <input type="text" class="form-control" disabled id="lastName" name="lastName" value="${(account.lastName!'')}"/>
            </div>
        </div>

        <!--
	<div class="form-group">
            <div class="col-sm-2 col-md-2">
                <label for="user.attributes.history" class="control-label">History</label>
            </div>

            <div class="col-sm-10 col-md-10">
                <input type="text" class="form-control" id="user.attributes.history" name="user.attributes.history" value="${(account.attributes.history!'')}"/>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-2 col-md-2">
                <label for="user.attributes.ethnicity" class="control-label">Ethnicity</label>
            </div>

            <div class="col-sm-10 col-md-10">
                <input type="text" class="form-control" id="user.attributes.ethnicity" name="user.attributes.ethnicity" value="${(account.attributes.ethnicity!'')}"/>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-2 col-md-2">
                <label for="user.attributes.gender" class="control-label">Gender</label>
            </div>

            <div class="col-sm-10 col-md-10">
                <input type="text" class="form-control" id="user.attributes.gender" name="user.attributes.gender" value="${(account.attributes.gender!'')}"/>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-2 col-md-2">
                <label for="user.attributes.ethnicity" class="control-label">Ethnicity</label>
            </div>

            <div class="col-sm-10 col-md-10">
                <input type="text" class="form-control" id="user.attributes.ethnicity" name="user.attributes.ethnicity" value="${(account.attributes.ethnicity!'')}"/>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-2 col-md-2">
                <label for="user.attributes.gender" class="control-label">Gender</label>
            </div>

            <div class="col-sm-10 col-md-10">
                <input type="text" class="form-control" id="user.attributes.gender" name="user.attributes.gender" value="${(account.attributes.gender!'')}"/>
            </div>
        </div>
        -->

        <div class="form-group">
            <div class="col-sm-2 col-md-2">
                <label for="user.attributes.storeurl" class="control-label">Store URL</label>
            </div>

            <div class="col-sm-10 col-md-10">
                <input type="text" class="form-control" disabled id="user.attributes.storeurl" name="user.attributes.storeurl" value="${(account.attributes.storeurl!'')}"/>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-2 col-md-2">
                <label for="user.attributes.mvturl" class="control-label">MVT URL</label>
            </div>

            <div class="col-sm-10 col-md-10">
                <input type="text" class="form-control" disabled id="user.attributes.mvturl" name="user.attributes.mvturl" value="${(account.attributes.mvturl!'')}"/>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-2 col-md-2">
                <label for="user.attributes.miturl" class="control-label">MIT URL</label>
            </div>

            <div class="col-sm-10 col-md-10">
                <input type="text" class="form-control" disabled id="user.attributes.miturl" name="user.attributes.miturl" value="${(account.attributes.miturl!'')}"/>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-2 col-md-2">
                <label for="user.attributes.rwurl" class="control-label">Regional Winner URL</label>
            </div>

            <div class="col-sm-10 col-md-10">
                <input type="text" class="form-control" disabled id="user.attributes.rwurl" name="user.attributes.rwurl" value="${(account.attributes.rwurl!'')}"/>
            </div>
        </div>



        <!--
        <div class="form-group">
            <div id="kc-form-buttons" class="col-md-offset-2 col-md-10 submit">
                <div class="">
                    <#if url.referrerURI??><a href="${url.referrerURI}">${kcSanitize(msg("backToApplication")?no_esc)}</a></#if>
                    <button type="submit" class="${properties.kcButtonClass!} ${properties.kcButtonPrimaryClass!} ${properties.kcButtonLargeClass!}" name="submitAction" value="Save">${msg("doSave")}</button>
                    <button type="submit" class="${properties.kcButtonClass!} ${properties.kcButtonDefaultClass!} ${properties.kcButtonLargeClass!}" name="submitAction" value="Cancel">${msg("doCancel")}</button>
                </div>
            </div>
        </div>
        -->
    </form>

</@layout.mainLayout>
