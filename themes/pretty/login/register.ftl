<#import "template.ftl" as layout>
<@layout.registrationLayout displayMessage=!messagesPerField.existsError('firstName','lastName','email','username','password','password-confirm'); section>
    <#if section = "header">
        ${msg("registerTitle")}
    <#elseif section = "form">
        
        <form id="kc-register-form" class="${properties.kcFormClass!}" action="${url.registrationAction}" method="post">
            <div class="${properties.kcFormGroupClass!}">
                <div class="${properties.kcLabelWrapperClass!}">
                    <label for="firstName" class="${properties.kcLabelClass!}">${msg("firstName")}</label>
                </div>
                <div class="${properties.kcInputWrapperClass!}">
                    <input type="text" id="firstName" class="${properties.kcInputClass!}" name="firstName"
                           value="${(register.formData.firstName!'')}"
                           aria-invalid="<#if messagesPerField.existsError('firstName')>true</#if>"
                    />

                    <#if messagesPerField.existsError('firstName')>
                        <span id="input-error-firstname" class="${properties.kcInputErrorMessageClass!}" aria-live="polite">
                            ${kcSanitize(messagesPerField.get('firstName'))?no_esc}
                        </span>
                    </#if>
                </div>
            </div>

            <div class="${properties.kcFormGroupClass!}">
                <div class="${properties.kcLabelWrapperClass!}">
                    <label for="lastName" class="${properties.kcLabelClass!}">${msg("lastName")}</label>
                </div>
                <div class="${properties.kcInputWrapperClass!}">
                    <input type="text" id="lastName" class="${properties.kcInputClass!}" name="lastName"
                           value="${(register.formData.lastName!'')}"
                           aria-invalid="<#if messagesPerField.existsError('lastName')>true</#if>"
                    />

                    <#if messagesPerField.existsError('lastName')>
                        <span id="input-error-lastname" class="${properties.kcInputErrorMessageClass!}" aria-live="polite">
                            ${kcSanitize(messagesPerField.get('lastName'))?no_esc}
                        </span>
                    </#if>
                </div>
            </div>
            <div class="${properties.kcFormGroupClass!} ${messagesPerField.printIfExists('comp_role',properties.kcFormGroupErrorClass!)}">
                <div class="${properties.kcLabelWrapperClass!}">
                    <label for="comp_role" class="${properties.kcLabelClass!}">Competition Role</label>
                </div>
                <div class="${properties.kcInputWrapperClass!}">
                    <select
                        id="user.attributes.comp_role"
                        class="${properties.kcInputClass!}"
                        name="user.attributes.comp_role"
                        value="${(register.formData['user.attributes.comp_role']!'')}">
                            <option value="Participant">Student Participant</option>
                            <option value="CAE-C Faculty">CAE-C Faculty</option>
                            <option value="Observer/Contributor">Observer/Contributor</option>
                    </select>
                </div>
            </div>
            <div class="${properties.kcFormGroupClass!}">
                <div class="${properties.kcLabelWrapperClass!}">
                    <label for="email" class="${properties.kcLabelClass!}">${msg("email")}</label>
                    <p class="light">Please use ".edu" addresses only.</p>
                </div>
                <div class="${properties.kcInputWrapperClass!}">
                    <input type="text" pattern="[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.(edu|org|gov)$" title="Email must end with.edu" id="email" class="${properties.kcInputClass!}" name="email"
                           value="${(register.formData.email!'')}" autocomplete="email"
                           aria-invalid="<#if messagesPerField.existsError('email')>true</#if>"
                           required
                    />
    
                    <#if messagesPerField.existsError('email')>
                        <span id="input-error-email" class="${properties.kcInputErrorMessageClass!}" aria-live="polite">
                            ${kcSanitize(messagesPerField.get('email'))?no_esc}
                        </span>
                    </#if>
                </div>
            </div>

            <#if !realm.registrationEmailAsUsername>
                <div class="${properties.kcFormGroupClass!}">
                    <div class="${properties.kcLabelWrapperClass!}">
                        <label for="username" class="${properties.kcLabelClass!}">${msg("username")}</label>
                    </div>
                    <div class="${properties.kcInputWrapperClass!}">
                        <input type="text" id="username" class="${properties.kcInputClass!}" name="username"
                               value="${(register.formData.username!'')}" autocomplete="username"
                               aria-invalid="<#if messagesPerField.existsError('username')>true</#if>"
                        />

                        <#if messagesPerField.existsError('username')>
                            <span id="input-error-username" class="${properties.kcInputErrorMessageClass!}" aria-live="polite">
                                ${kcSanitize(messagesPerField.get('username'))?no_esc}
                            </span>
                        </#if>
                    </div>
                </div>
            </#if>

            <#if passwordRequired??>
                <div class="${properties.kcFormGroupClass!}">
                    <div class="${properties.kcLabelWrapperClass!}">
                        <label for="password" class="${properties.kcLabelClass!}">${msg("password")}</label>
                    </div>
                    <div class="${properties.kcInputWrapperClass!}">
                        <input type="password" id="password" class="${properties.kcInputClass!}" name="password"
                               autocomplete="new-password"
                               aria-invalid="<#if messagesPerField.existsError('password','password-confirm')>true</#if>"
                        />

                        <#if messagesPerField.existsError('password')>
                            <span id="input-error-password" class="${properties.kcInputErrorMessageClass!}" aria-live="polite">
                                ${kcSanitize(messagesPerField.get('password'))?no_esc}
                            </span>
                        </#if>
                    </div>
                </div>

                <div class="${properties.kcFormGroupClass!}">
                    <div class="${properties.kcLabelWrapperClass!}">
                        <label for="password-confirm"
                               class="${properties.kcLabelClass!}">${msg("passwordConfirm")}</label>
                    </div>
                    <div class="${properties.kcInputWrapperClass!}">
                        <input type="password" id="password-confirm" class="${properties.kcInputClass!}"
                               name="password-confirm"
                               aria-invalid="<#if messagesPerField.existsError('password-confirm')>true</#if>"
                        />

                        <#if messagesPerField.existsError('password-confirm')>
                            <span id="input-error-password-confirm" class="${properties.kcInputErrorMessageClass!}" aria-live="polite">
                                ${kcSanitize(messagesPerField.get('password-confirm'))?no_esc}
                            </span>
                        </#if>
                    </div>
                </div>
            </#if>

            <#if recaptchaRequired??>
                <div class="form-group">
                    <div class="${properties.kcInputWrapperClass!}">
                        <div class="g-recaptcha" data-size="compact" data-sitekey="${recaptchaSiteKey}"></div>
                    </div>
                </div>
            </#if>
            <div class="${properties.kcFormGroupClass!}">
                <div class="${properties.kcLabelWrapperClass!}">
                    <label for="user.attributes.prior_ctf" class="${properties.kcLabelClass!}">Have you ever participated in a CTF-style competition?</label>
                </div>

                <div class="${properties.kcInputWrapperClass!}">
                    <label>Yes</label>
                    <input type="radio" id="user.attributes.prior_ctf" name="user.attributes.prior_ctf" value="True">
                    <label>No</label>
                    <input type="radio" id="user.attributes.prior_ctf" name="user.attributes.prior_ctf" value="False" checked>
                </div>
            </div>
            <div class="${properties.kcFormGroupClass!}">
                <div class="${properties.kcLabelWrapperClass!}">
                    <label for="user.attributes.prior_infrastructure" class="${properties.kcLabelClass!}">Have you participated in a service-based infrastructure competition like this before?</label>
                </div>

                <div class="${properties.kcInputWrapperClass!}">
                    <label>Yes</label>
                    <input type="radio" id="user.attributes.prior_infrastructure" name="user.attributes.prior_infrastructure" value="True">
                    <label>No</label>
                    <input type="radio" id="user.attributes.prior_infrastructure" name="user.attributes.prior_infrastructure" value="False" checked>
                </div>
            </div>
            <div class="${properties.kcFormGroupClass!}">
                <div class="${properties.kcLabelWrapperClass!}">
                    <label for="user.attributes.related_classes" class="${properties.kcLabelClass!}">How many cybersecurity-related college / university-level classes have you completed?</label>
                </div>
                <div class="${properties.kcInputWrapperClass!}">
                    <select
                        id="user.attributes.related_classes"
                        class="${properties.kcInputClass!}"
                        name="user.attributes.related_classes"
                        value="${(register.formData['user.attributes.related_classes']!'')}">
                            <option value="0">0</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                            <option value="11">11</option>
                            <option value="12+">12+</option>
                    </select>
                </div>
            </div>
            <div class="${properties.kcFormGroupClass!}">
                <div class="${properties.kcLabelWrapperClass!}">
                    <label for="user.attributes.years_experience" class="${properties.kcLabelClass!}">How many years of experience do you have working in cybersecurity or a cybersecurity-related field?</label>
                </div>
                <div class="${properties.kcInputWrapperClass!}">
                    <select
                        id="user.attributes.years_experience"
                        class="${properties.kcInputClass!}"
                        name="user.attributes.years_experience"
                        value="${(register.formData['user.attributes.years_experience']!'')}">
                            <option value="0">0</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4+">4+</option>
                    </select>
                </div>
            </div>
            <div style="margin-top: 15px; margin-bottom: 15px; padding: 5px; border-width: 1px; border-style: solid;">
                <label>We're passionate about improving the diversity of those in the cyber community! This competition aims to welcome all demographics. To help us understand better how we're doing, we ask that you provide the following information. It is completely optional, will have no impact on your experience in the competition, and will never be sold or used to personally identify you.</label>
            </div>
            <div class="${properties.kcFormGroupClass!}">
                <div class="${properties.kcLabelWrapperClass!}">
                    <label for="user.attributes.age" class="${properties.kcLabelClass!}">Age</label>
                </div>
                <div class="${properties.kcInputWrapperClass!}">
                    <input type="number" class="${properties.kcInputClass!}" id="user.attributes.age" name="user.attributes.age" value="${(register.formData['user.attributes.age']!'')}" required/>
                </div>
            </div>
            <div class="${properties.kcFormGroupClass!}">
                <div class="${properties.kcLabelWrapperClass!}">
                    <label for="user.attributes.ethnicity" class="${properties.kcLabelClass!}">Select all of the following you identify with most</label>
                </div>
                <div class="${properties.kcInputWrapperClass!}">
                    <div class="checkbox" style="padding-left: 10px;">
                        <input style="margin-left: 0px;" type="checkbox" id="user.attributes.ethnicity-american" name="user.attributes.ethnicity" value="American Indian or Alaska Native" />
                        <label class="" for="user.attributes.ethnicity-american">American Indian or Alaska Native</label>
                    </div>
                    <div class="checkbox" style="padding-left: 10px;">
                        <input style="margin-left: 0px;" type="checkbox" id="user.attributes.ethnicity-asian" name="user.attributes.ethnicity" value="Asian" />
                        <label class="" for="user.attributes.ethnicity-asian">Asian</label>
                    </div>
                    <div class="checkbox" style="padding-left: 10px;">
                        <input style="margin-left: 0px;" type="checkbox" id="user.attributes.ethnicity-black" name="user.attributes.ethnicity" value="Black or African American" />
                        <label class="" for="user.attributes.ethnicity-black">Black or African American</label>
                    </div>
                    <div class="checkbox" style="padding-left: 10px;">
                        <input style="margin-left: 0px;" type="checkbox" id="user.attributes.ethnicity-hispanic" name="user.attributes.ethnicity" value="Hispanic or Latino" />
                        <label class="" for="user.attributes.ethnicity-hispanic">Hispanic or Latino</label>
                    </div>
                    <div class="checkbox" style="padding-left: 10px;">
                        <input style="margin-left: 0px;" type="checkbox" id="user.attributes.ethnicity-native" name="user.attributes.ethnicity" value="Native Hawaiian or Other Pacific Islander" />
                        <label class="" for="user.attributes.ethnicity-native">Native Hawaiian or Other Pacific Islander</label>
                    </div>
                    <div class="checkbox" style="padding-left: 10px;">
                        <input style="margin-left: 0px;" type="checkbox" id="user.attributes.ethnicity-white" name="user.attributes.ethnicity" value="White" />
                        <label class="" for="user.attributes.ethnicity-white">White</label>
                    </div>
                    <div class="checkbox" style="padding-left: 10px;">
                        <input style="margin-left: 0px;" type="checkbox" id="user.attributes.ethnicity-prefer-not-to-answer" name="user.attributes.ethnicity" value="Prefer not to answer" />
                        <label class="" for="user.attributes.ethnicity-prefer-not-to-answer">Prefer not to answer</label>
                    </div>
                    <div class="checkbox" style="padding-left: 10px;">
                        <input style="margin-left: 0px;" type="checkbox" id="user.attributes.ethnicity-none" name="user.attributes.ethnicity" value="None" />
                        <label class="" for="user.attributes.ethnicity-none">None of the above</label>
                    </div>
                </div>
            </div>
            <div class="${properties.kcFormGroupClass!}">
                <div class="${properties.kcLabelWrapperClass!}">
                    <label for="user.attributes.gender" class="${properties.kcLabelClass!}">Select the term(s) with which you identify with most</label>
                </div>
                <div class="${properties.kcInputWrapperClass!}">
                    <div class="checkbox" style="padding-left: 10px;">
                        <input style="margin-left: 0px;" type="checkbox" id="user.attributes.gender-man" name="user.attributes.gender" value="Man" />
                        <label for="man">Man</label>
                    </div>
                    <div class="checkbox" style="padding-left: 10px;">
                        <input style="margin-left: 0px;" type="checkbox" id="user.attributes.gender-woman" name="user.attributes.gender" value="Woman" />
                        <label for="woman">Woman</label>
                    </div>
                    <div class="checkbox" style="padding-left: 10px;">
                        <input style="margin-left: 0px;" type="checkbox" id="user.attributes.gender-agender" name="user.attributes.gender" value="Agender" />
                        <label for="agender">Agender</label>
                    </div>
                    <div class="checkbox" style="padding-left: 10px;">
                        <input style="margin-left: 0px;" type="checkbox" id="user.attributes.gender-non-binary" name="user.attributes.gender" value="Non-Binary" />
                        <label for="non-binary">Non-Binary</label>
                    </div>
                    <div class="checkbox" style="padding-left: 10px;">
                        <input style="margin-left: 0px;" type="checkbox" id="user.attributes.gender-demigender" name="user.attributes.gender" value="Demigender" />
                        <label for="demigender">Demigender</label>
                    </div>
                    <div class="checkbox" style="padding-left: 10px;">
                        <input style="margin-left: 0px;" type="checkbox" id="user.attributes.gender-genderfluid" name="user.attributes.gender" value="Genderfluid" />
                        <label for="genderfluid">Genderfluid</label>
                    </div>
                    <div class="checkbox" style="padding-left: 10px;">
                        <input style="margin-left: 0px;" type="checkbox" id="user.attributes.gender-genderqueer" name="user.attributes.gender" value="Genderqueer" />
                        <label for="genderqueer">Genderqueer</label>
                    </div>
                    <div class="checkbox" style="padding-left: 10px;">
                        <input style="margin-left: 0px;" type="checkbox" id="user.attributes.gender-pangender" name="user.attributes.gender" value="Pangender" />
                        <label for="pangender">Pangender</label>
                    </div>
                    <div class="checkbox" style="padding-left: 10px;">
                        <input style="margin-left: 0px;" type="checkbox" id="user.attributes.gender-prefer-not-to-answer" name="user.attributes.gender" value="Prefer Not to Answer" />
                        <label for="prefer-not-to-answer">Prefer Not to Answer</label>
                    </div>
                    <div class="checkbox" style="padding-left: 10px;">
                        <!-- Dummy input for consistency -->
                        <label for="user.attributes.gender-self-describe" class="${properties.kcLabelClass!}">Self-describe</label>
                    </div>
                    <div class="${properties.kcInputWrapperClass!}">
                        <input type="text" class="${properties.kcInputClass!}" id="user.attributes.gender-self-describe" name="user.attributes.gender" value="${(register.formData['user.attributes.gender']!'')}" />
                    </div>
                </div>
            </div>
            <script type="text/javascript">
                function returnToPreviousPage() {
                    window.history.back();
                }
                function valthisform()
                {
                    var ethnicity_checkboxs=document.getElementsByName("user.attributes.ethnicity");
                    var okay=false;
                    for(var i=0,l=ethnicity_checkboxs.length;i<l;i++)
                    {
                        if(ethnicity_checkboxs[i].checked)
                        {
                            okay=true;
                            break;
                        }
                    }
                    if(!okay)
                    {
                        alert("Please select an ethnicity option")
                        return false;
                    }
                    okay=false;
                    var gender_self_describe=document.getElementById("user.attributes.gender-self-describe");
                    if (gender_self_describe.value.length > 64) {
                        alert("Please restrict gender self description to 64 characters or less")
                        return false;
                    }
                    if (gender_self_describe.value.length > 0) {
                        return true;
                    } else {
                        gender_self_describe.disabled=true;
                    }
                    var gender_checkboxs=document.getElementsByName("user.attributes.gender");
                    for(var i=0,l=gender_checkboxs.length;i<l;i++)
                    {
                        if(gender_checkboxs[i].checked)
                        {
                            okay=true;
                            break;
                        }
                    }
                    if(okay)return true;
                    else
                    {
                        alert("Please select a gender option")
                        return false;
                    }
                }
            </script>
            <div class="${properties.kcFormGroupClass!}">
                <div id="kc-form-options" class="${properties.kcFormOptionsClass!}">
                    <div class="${properties.kcFormOptionsWrapperClass!}">
                        <span><a href="${url.loginUrl}">${kcSanitize(msg("backToLogin"))?no_esc}</a></span>
                    </div>
                </div>

                <div id="kc-form-buttons" class="${properties.kcFormButtonsClass!}">
                    <input class="${properties.kcButtonClass!} ${properties.kcButtonPrimaryClass!} ${properties.kcButtonBlockClass!} ${properties.kcButtonLargeClass!}" type="submit" onclick="return valthisform();" value="${msg("doRegister")}"/>
                </div>
            </div>
        </form>
    </#if>
</@layout.registrationLayout>